
const cb = function(x) { 
	return x * x;
 }

 
function cacheFunction(cb) {
	const cache = {};

const checkArgument = function(argument) {
  
  if(argument in cache) {
	  console.log("tracking Object")
	return cache[argument];
 }
  else {
	cache[argument] = cb(argument);
	console.log("Invoked CB")
	return cb(argument);
 }
}

console.log(checkArgument(5))
console.log(checkArgument(4))
console.log(checkArgument(3))
console.log(checkArgument(2))
console.log(checkArgument(5))
console.log(cache)



return checkArgument
}

module.exports = {cacheFunction,cb}

